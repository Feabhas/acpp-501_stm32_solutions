// ----------------------------------------------------------------------------
//
// Feabhas STM32F407VG target project
//
// Advanced C++ for Real-Time Embedded Developers
// Exercise 1 Solution
//
// ----------------------------------------------------------------------------

#include <iostream>

#include "Timer.h"
#include "Buffer.h"

using namespace std;

int main()
{
  Buffer buffer;

  Buffer::Error err;

  for (int i = 0; i < 10; ++i)
  {
    err = buffer.add(i);
    if (err == Buffer::OK)
    {
      cout << "OK" << endl;
    }
    else
    {
      cout << "Buffer full" << endl;
    }
  }

  for (int i = 0; i < 10; ++i)
  {
    int data;
    err = buffer.get(data);
    if (err == Buffer::OK)
    {
      cout << "OK - data is " << data << endl;
    }
    else
    {
      cout << "EMPTY - data is " << data << endl;
    }
  }
}

// ----------------------------------------------------------------------------
