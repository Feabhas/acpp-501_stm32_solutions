// ----------------------------------------------------------------------------
//
// Feabhas STM32F407VG target project
//
// A basic main, showing the  supplied timer
// and trace output functions.
//
// ----------------------------------------------------------------------------

#include <stdio.h>


#include "GPIO.h"
#include "SevenSegment.h"
#include "Motor.h"

int main()
{
  // Basic GPIO control
  //
  Pin D6(Peripheral::GPIO_D, 8,  Pin::OUTPUT);
  Pin D5(Peripheral::GPIO_D, 9,  Pin::OUTPUT);
  Pin D4(Peripheral::GPIO_D, 10, Pin::OUTPUT);
  Pin D3(Peripheral::GPIO_D, 11, Pin::OUTPUT);

  D5 = 1;
  D3 = 1;

  // Seven-segment display
  //
  SevenSegment programStatus;
  programStatus.display(4);

  // Motor
  //
  Motor motor;
  motor.on();
  motor.direction(Motor::ACW);
  motor.off();
}

// ----------------------------------------------------------------------------
