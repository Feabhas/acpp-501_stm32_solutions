// ----------------------------------------------------------------------------------
// WashProgramme.cpp
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include "WashProgramme.h"
#include "Step.h"

WashProgramme::WashProgramme() :
  numSteps(0)
{
  for(unsigned int i = 0; i < sz; ++i) steps[i] = 0;
}


bool WashProgramme::add(Step& step)
{
  if (numSteps == sz) return false;

  steps[numSteps] = &step;
  ++numSteps;

  return true;
}


void WashProgramme::run()
{
  for(unsigned i = 0; i < numSteps; ++i)
  {
    if(steps[i] != 0) steps[i]->run();
  }
}
