// ----------------------------------------------------------------------------
//
// Feabhas STM32F407VG target project
//
// A basic main, showing the  supplied timer
// and trace output functions.
//
// ----------------------------------------------------------------------------

#include <stdio.h>

#include "SevenSegment.h"
#include "Step.h"
#include "WashProgramme.h"
#include "WMS.h"
#include "Timer.h"
#include "ProgramKeys.h"

int main()
{
  // Initialise the sysTick timer (required by
  // the Steps)
  //
  Timer sysTick;
  sysTick.start();

  SevenSegment statusIndicator;
  ProgramKeys selector;

  // Create the step instances.  These should be the only
  // instances of the Steps in the system.
  //
  Step empty(Step::EMPTY, statusIndicator);
  Step fill(Step::FILL, statusIndicator);
  Step heat(Step::HEAT, statusIndicator);
  Step wash(Step::WASH, statusIndicator);
  Step rinse(Step::RINSE, statusIndicator);
  Step spin(Step::SPIN, statusIndicator);
  Step dry(Step::DRY, statusIndicator);
  Step complete(Step::COMPLETE, statusIndicator);

  // WashProgramme, as a sequence of Steps
  //
  WashProgramme colour;
  colour.add(fill);
  colour.add(heat);
  colour.add(wash);
  colour.add(empty);
  colour.add(fill);
  colour.add(rinse);
  colour.add(empty);
  colour.add(spin);
  colour.add(dry);
  colour.add(complete);

  WMS wms;
  wms.set(WMS::COLOUR, colour);

  unsigned selection = selector.getValue();
  wms.run(static_cast<WMS::Programme>(selection));
}

// ----------------------------------------------------------------------------
