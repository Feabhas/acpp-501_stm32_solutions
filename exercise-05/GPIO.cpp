// ----------------------------------------------------------------------------------
// peripherals.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include "GPIO.h"
#include "memory_map.h"
#include <cassert>

#if __cplusplus<201103L
  #include <stdint.h>
#else
  #include <cstdint>
  using std::uint32_t;
#endif


// ---------------------------------------------------------------
// Structure overlay for register access.
// The GPIO ports are at fixed offsets from the start of the AHB1
// bus.  Each port is at an offset of 0x400 from the previous; and
// the ports are in port order (Port_A -> Port_I).
// The AHB1 base address is defined in memory_map.h

// All registers are 32 bits in size
//
typedef uint32_t register_t;

struct Pin::PortRegisters
{
  register_t MODE;
  register_t OTYPE;
  register_t OSPEED;
  register_t PUPD;
  register_t ID;
  register_t OD;
  register_t BSR;
  register_t LCK;
  register_t AFRL;
  register_t AFRH;
};


Pin::Pin(Peripheral::AHB1_Device dev, unsigned pin_number, Mode m) :
    port(reinterpret_cast<PortRegisters*>(AHB1_BASE + (0x400 * dev))),
    pin(pin_number)
{
  assert(pin < 16);

  // Enable the clock to the GPIO port
  //
  Peripheral::enable(dev);

  // Default configuration.  This is good enough for
  // basic GPIO but will have to be changed for any
  // other functionality
  //
  mode(m);
  output_type(Pin::PUSH_PULL);
  output_speed(Pin::LOW);
  push_pull(Pin::NO_PUSH_PULL);Peripheral::enable(dev);
}


void Pin::set()
{
  port->OD |= (1 << pin);
}


void Pin::clear()
{
  port->OD &= ~(1 << pin);
}


bool Pin::isSet()
{
  return ((port->ID & (1 << pin)) != 0);
}


void Pin::mode(Mode m)
{
  port->MODE &= ~(0x03 << (pin * 2));
  port->MODE |=  (m    << (pin * 2));
}


void Pin::output_type(OutputType type)
{
  port->OTYPE &= ~(0x01 << pin);
  port->OTYPE |=  (type << pin);
}


void Pin::output_speed(OutputSpeed speed)
{
  port->OSPEED &= ~(0x03  << (pin * 2));
  port->OSPEED |=  (speed << (pin * 2));
}


void Pin::push_pull(PushPull pupd)
{
  port->PUPD &= ~(0x03 << (pin * 2));
  port->PUPD |=  (pupd << (pin * 2));
}

void Pin::alternative_function(AltFunction fn)
{
  if(pin < 8)
  {
    port->AFRL &= ~(0x0F << (4 * (pin % 8)));
    port->AFRL |=  (fn   << (4 * (pin % 8)));
  }
  else
  {
    port->AFRH &= ~(0x0F << (4 * (pin % 8)));
    port->AFRH |=  (fn   << (4 * (pin % 8)));
  }
}

