// ----------------------------------------------------------------------------------
// SevenSegment.cpp
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include "SevenSegment.h"

SevenSegment::SevenSegment() :
  A(Peripheral::GPIO_D,  8, Pin::OUTPUT),
  B(Peripheral::GPIO_D,  9, Pin::OUTPUT),
  C(Peripheral::GPIO_D, 10, Pin::OUTPUT),
  D(Peripheral::GPIO_D, 11, Pin::OUTPUT)
{
  blank();
}


void SevenSegment::display(uint32_t val)
{
  A = (val & 0x01);
  B = (val & 0x02);
  C = (val & 0x04);
  D = (val & 0x08);
}


void SevenSegment::blank()
{
  display(15);
}
