// ----------------------------------------------------------------------------
//
// Feabhas STM32F407VG target project
//
// A basic main, showing the  supplied timer
// and trace output functions.
//
// ----------------------------------------------------------------------------

#include <stdio.h>

#include "SevenSegment.h"
#include "Motor.h"
#include "Step.h"
#include "MotorStep.h"
#include "WashProgramme.h"
#include "WMS.h"
#include "Timer.h"
#include "ProgramKeys.h"

int main()
{
  // Initialise the sysTick timer (required by
  // the Steps)
  //
  Timer sysTick;
  sysTick.start();

  SevenSegment statusIndicator;
  ProgramKeys selector;
  Motor motor;

  // Create the step instances.  These should be the only
  // instances of the Steps in the system.
  //
  Step empty(Step::EMPTY);
  Step fill(Step::FILL);
  Step heat(Step::HEAT);
  WashStep wash(Step::WASH);
  Step rinse(Step::RINSE);
  SpinStep spin(Step::SPIN);
  Step dry(Step::DRY);
  Step complete(Step::COMPLETE);

  // Bind the Steps to their output devices
  //
  connect(empty, statusIndicator);
  connect(fill, statusIndicator);
  connect(heat, statusIndicator);
  connect(wash, statusIndicator);
  connect(wash, motor);
  connect(rinse, statusIndicator);
  connect(spin, statusIndicator);
  connect(spin, motor);
  connect(dry, statusIndicator);
  connect(complete, statusIndicator);

  // WashProgramme, as a sequence of Steps
  //
  WashProgramme colour;
  colour.add(fill);
  colour.add(heat);
  colour.add(wash);
  colour.add(empty);
  colour.add(fill);
  colour.add(rinse);
  colour.add(empty);
  colour.add(spin);
  colour.add(dry);
  colour.add(complete);

  WMS wms;
  wms.set(WMS::COLOUR, colour);

  unsigned selection = selector.getValue();
  wms.run(static_cast<WMS::Programme>(selection));
}

// ----------------------------------------------------------------------------
