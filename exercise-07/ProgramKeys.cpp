// ----------------------------------------------------------------------------------
// ProgramKeys.cpp
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include "ProgramKeys.h"

ProgramKeys::ProgramKeys() :
  accept(Peripheral::GPIO_D, 5, Pin::INPUT),
  cancel(Peripheral::GPIO_D, 4, Pin::INPUT),
  ps1   (Peripheral::GPIO_D, 1, Pin::INPUT),
  ps2   (Peripheral::GPIO_D, 2, Pin::INPUT),
  ps3   (Peripheral::GPIO_D, 3, Pin::INPUT),
  latch (Peripheral::GPIO_D, 14, Pin::OUTPUT)
{
}


unsigned int ProgramKeys::getValue()
{
  // Set the latch, to capture PS selections.
  // If the user presses CANCEL drop, then reset the latch.
  // If ACCEPT is pressed drop the latch then wait for
  // the ACCEPT key to be *released*
  //
  unsigned int value = 0;

  latch = 1;
  do
  {
    if(cancel)  // Reset the latch
    {
      latch = 0;
      latch = 1;
      value = 0;
    }

    if(ps1) value |= 0x01;
    if(ps2) value |= 0x02;
    if(ps3) value |= 0x04;

  } while (!accept);

  latch = 0;
  while(accept); // Wait for ACCEPT release

  return value;
}

