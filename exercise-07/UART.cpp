// ----------------------------------------------------------------------------------
// UART.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include "UART.h"


// -----------------------------------------------------------------
// UART class definitions
//

struct UART::UARTRegisters
  {
    unsigned SR;    /*!< USART Status register,                   Address offset: 0x00 */
    unsigned DR;    /*!< USART Data register,                     Address offset: 0x04 */
    unsigned BRR;   /*!< USART Baud rate register,                Address offset: 0x08 */
    unsigned CR1;   /*!< USART Control register 1,                Address offset: 0x0C */
    unsigned CR2;   /*!< USART Control register 2,                Address offset: 0x10 */
    unsigned CR3;   /*!< USART Control register 3,                Address offset: 0x14 */
    unsigned GTPR;  /*!< USART Guard time and prescaler register, Address offset: 0x18 */
  };

UART::UART(UART_Address addr) :
  uart(reinterpret_cast<UARTRegisters*>(addr)),
  Rx_buffer()
{
  // Set up the callback pointer
  //
  self = this;

  // Stop the USART before configuring
  //
  uart->CR1 &= ~(1 << 13);

  // Enable the USART clock.  Since this operation is
  // specific to a particular device we should not hard-code
  // a call here.
  //
  Peripheral::enable(Peripheral::USART_3);

  // Reset STOP bits
  // USART3->CR2 &= ~(3 << 12);   // Clear STOP[13:12] bits

  // Setup 8,N,1
  //
  uart->CR1 |= ((1 << 2) | (1 << 3)); // Set TE and RE bits

  // 115.2kb based on 16MHz clk and 16x oversampling
  //
  uart->BRR = 0x8B;

  // Configure the Tx / Rx pins for the device
  //
  remap_IO_Pins();

  //  Enable interrupt on Rx
  //
  uart->CR1 |= (1 << 5);

  // Enable interrupts for this particular device.
  // As previously, this should be adaptable for
  // different UARTs
  //
  NVIC_EnableIRQ(USART3_IRQn);

  // Enable the USART
  //
  uart->CR1 |= (1 << 13);
}


UART::~UART()
{
  // Stop the USART
  //
  uart->CR1 &= ~(1 << 13);
}


void UART::putChar(char c)
{
  while ((uart->SR & (1 << 7)) == 0)
  {
    ; // Wait...
  }
  uart->DR = c;
}


char UART::getChar()
{
  char chr;
  Rx_Buffer_t::Error error;
  do
  {
    NVIC_DisableIRQ(USART3_IRQn);    // NOTE: Specific to a particular UART!

    error = Rx_buffer.get(chr);

    NVIC_EnableIRQ(USART3_IRQn);
  } while(error != Rx_Buffer_t::OK);

  return chr;
}


void UART::putString(const char* str)
{
  while (*str != '\0')
  {
    this->putChar(*str++);
  }
}


void UART::remap_IO_Pins()
{
  // Each UART requires two GPIO pins to be reconfigured
  // to act as the Tx and Rx pins.  These pins are on a
  // different port, and indeed different pins, for each
  // UART.

  // Enable GPIO port clock
  //
  Peripheral::enable(Peripheral::GPIO_B);

  Pin Tx(Peripheral::GPIO_B, 10);
  Pin Rx(Peripheral::GPIO_B, 11);

  Tx.mode(Pin::ALT_FN);
  Tx.alternative_function(Pin::AF7);
  Tx.output_speed(Pin::HIGH);
  Tx.push_pull(Pin::PULL_UP);

  Rx.mode(Pin::ALT_FN);
  Rx.alternative_function(Pin::AF7);
  Rx.output_speed(Pin::HIGH);
  Rx.push_pull(Pin::PULL_UP);
}


// Definition of callback pointer
//
UART* UART::self = 0;


void UART::IRQHandler(void)
{
  uint8_t val = self->uart->DR;
  self->Rx_buffer.add(val);
}

// Interrupt handlers
//
extern "C"
void USART3_IRQHandler(void)
{
  UART::IRQHandler();
}

// Interface implementation
//
void UART::write(int value)
{
  putChar(value + '0');
}


void UART::write(const char* str)
{
  putString(str);
}


int  UART::read()
{
  return (getChar() - '0');
}
