// ----------------------------------------------------------------------------------
// UART.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#ifndef UART_H_
#define UART_H_

#include "stm32f4xx.h"
#include "peripherals.h"
#include "GPIO.h"
#include "Buffer.h"
#include "Interfaces.h"

class UART : public I_DigitOutput,
             public I_DigitInput,
             public I_TextOutput
{
public:
  // NOTE:  For this project we are only using UART 3
  //        DO NOT use any other UART!
  //
  enum UART_Address { UART_1, UART_2, UART_3 = 0x40004800, UART_4, UART_5, UART_6 };

  UART(UART_Address addr);
  ~UART();

  void putChar(char c);
  char getChar();
  void putString(const char* str);

  static void IRQHandler(void);

protected:
  // Interface implementations
  //
  virtual void write(int value);
  virtual void write(const char* str);
  virtual int  read();

  // NOTE:  These are free functions, NOT member
  //        functions.  However, they have been
  //        implemented inline.
  //        They could have also been implemented
  //        as member functions.
  //
  typedef UART& (*Modifier)(UART&);
  friend UART& operator<<(UART& uart, int in_val)     { uart.putChar(in_val); return uart; }
  friend UART& operator<<(UART& uart, const char* str){ uart.putString(str); return uart;  }
  friend void  operator>>(UART& uart, int& inout_val) { inout_val = uart.read();           }

  // For endl; (etc) - Known as 'modifiers'.
  // This is a basic implementation of the Visitor
  // pattern.
  //
  friend UART& operator<<(UART& uart, UART& (*modifier)(UART&)) { return modifier(uart); }

private:
  void remap_IO_Pins();

  // UART configuration registers
  //
  struct UARTRegisters;
  volatile UARTRegisters* uart;

  // Interrupt-driven UART
  //
  static UART* self;
  typedef Buffer Rx_Buffer_t;
  Rx_Buffer_t Rx_buffer;
};

// Modifier functions
//
UART& endl(UART& uart);


#endif /* UART_H_ */
