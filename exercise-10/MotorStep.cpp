// ----------------------------------------------------------------------------------
// MotorStep.cpp
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include "MotorStep.h"
#include "Motor.h"
#include "Timer.h"
#include <cassert>

void SpinStep::run()
{
  // If the motor association is empty that indicates
  // a logic error somewhere in our code.
  //
  assert(motor != 0);

  Step::run();
  motor->on();
  Timer::sleep(1000);
  motor->off();
}


void WashStep::run()
{
  assert(motor != 0);

  Step::run();

  motor->on();
  for(int i = 0; i < 4; ++i)
  {
    motor->direction(I_Motor::CW);
    Timer::sleep(250);
    motor->direction(I_Motor::ACW);
    Timer::sleep(250);
  }
  motor->off();
}

