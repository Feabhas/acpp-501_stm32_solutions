// ----------------------------------------------------------------------------------
// SevenSegment.h
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#ifndef SEVENSEGMENT_H_
#define SEVENSEGMENT_H_

#include "GPIO.h"
#include "Interfaces.h"

#if __cplusplus<201103L
  #include <stdint.h>
#else
  #include <cstdint>
  using std::uint32_t;
#endif


class SevenSegment : public I_DigitOutput
{
public:
  SevenSegment();
  void display(uint32_t val);
  void blank();

protected:
  virtual void write(int value) { display(value); }

private:
  Pin A;
  Pin B;
  Pin C;
  Pin D;
};




#endif /* SEVENSEGMENT_H_ */
