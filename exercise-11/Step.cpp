// ----------------------------------------------------------------------------------
// Motor.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include "Step.h"
#include "SevenSegment.h"
#include "Timer.h"
#include "Interfaces.h"
#include <cassert>

// Output the Step's type as a string
// to test text output.
//
static const char* strings[] =
{ "INVALID", "EMPTY", "FILL", "HEAT", "WASH", "RINSE", "SPIN", "DRY", "COMPLETE" };

static const char* asString(Step::StepType step)
{
  return strings[step];
}


void Step::run()
{
  // A Step must always be associated with a display; if
  // not, there is a logic error somewhere in the code
  //
  assert(display != 0);

  display->write(stepNumber);

  I_TextOutput* textOutputter = dynamic_cast<I_TextOutput*>(display);
  if(textOutputter != 0)
  {
    textOutputter->write(asString(stepNumber));
    textOutputter->write("\n\r");
  }

  Timer::sleep(1000);
}

