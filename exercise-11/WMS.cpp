// ----------------------------------------------------------------------------------
// WMS.cpp
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include "WMS.h"
#include "WashProgramme.h"
#include "Exceptions.h"

using std::map;
using std::pair;

WMS::WMS() :
  progs()
{
}


void WMS::set(WMS::Programme prog, WashProgramme& wash)
{
  progs.insert(Prog_map::value_type(prog, &wash));
}


void WMS::run(WMS::Programme prog)
{
  Prog_map::iterator it = progs.find(prog);

  if(it == progs.end()) throw Exception("Programme not found");

  it->second->run();
}

