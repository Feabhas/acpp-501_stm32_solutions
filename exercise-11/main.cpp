// ----------------------------------------------------------------------------
//
// Feabhas STM32F407VG target project
//
// A basic main, showing the  supplied timer
// and trace output functions.
//
// ----------------------------------------------------------------------------

#include <stdio.h>

#include "SevenSegment.h"
#include "Motor.h"
#include "Step.h"
#include "MotorStep.h"
#include "WashProgramme.h"
#include "WMS.h"
#include "Timer.h"
#include "ProgramKeys.h"
#include "UART.h"
#include "UI.h"
#include "Exceptions.h"
#include "CheckedMotor.h"


int main()
{
  // Initialise the sysTick timer (required by
  // the Steps)
  //
  Timer sysTick;
  sysTick.start();

  SevenSegment statusIndicator;
  ProgramKeys selector;
  CheckedMotor motor;
  UART<UART_3> ttyUSB0;  // <= How the name appears on Linux

  // User interface
  //
  UI ui;
  connect(ui, ttyUSB0);

  ttyUSB0 << "Starting up..." << endl;

  // Create the step instances.  These should be the only
  // instances of the Steps in the system.
  //
  ttyUSB0 << "Creating Steps..." << endl;
  //ttyUSB0.putString("Creating Steps...\n\r");

  Step empty(Step::EMPTY);
  Step fill(Step::FILL);
  Step heat(Step::HEAT);
  WashStep wash(Step::WASH);
  Step rinse(Step::RINSE);
  SpinStep spin(Step::SPIN);
  Step dry(Step::DRY);
  Step complete(Step::COMPLETE);

  // Bind the Steps to their output devices
  //
  connect(empty, statusIndicator);
  connect(fill, statusIndicator);
  connect(heat, statusIndicator);
  connect(wash, ttyUSB0);
  connect(wash, motor);
  connect(rinse, statusIndicator);
  connect(spin, ttyUSB0);
  connect(spin, motor);
  connect(dry, statusIndicator);
  connect(complete, statusIndicator);

  // WashProgramme, as a sequence of Steps
  //
  ttyUSB0 << "Creating Programmes..." << endl;
  //ttyUSB0.putString("Creating Programmes...\n\r");

  WashProgramme colour;
  colour.add(fill);
  colour.add(heat);
  colour.add(wash);
  colour.add(empty);
  colour.add(fill);
  colour.add(rinse);
  colour.add(empty);
  colour.add(spin);
  colour.add(dry);
  colour.add(complete);

  WMS wms;
  wms.set(WMS::COLOUR, colour);

  unsigned selection = ui.run();
  ttyUSB0 << "Running wash: " << (selection + '0') << endl;
  //ttyUSB0.putString("Running wash...\n\r");

  try
  {
    wms.run(static_cast<WMS::Programme>(selection));
  }
  catch(Exception& ex)
  {
    ttyUSB0 << ex.what() << endl;
  }

  ttyUSB0 << "DONE!"  << endl;
}

// ----------------------------------------------------------------------------
