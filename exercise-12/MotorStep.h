// ----------------------------------------------------------------------------------
// MotorStep.h
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#ifndef MOTORSTEP_H_
#define MOTORSTEP_H_

#include "Step.h"

namespace WashingMachine
{
  class I_Motor;

  // Abstract Base Class for
  // motor-driving Steps
  //
  class MotorStep: public Step
  {
  public:
    MotorStep(Step::StepType step) : Step(step), motor(0) {}
    virtual void run() = 0;

  protected:
    friend void connect(MotorStep& step, I_Motor& m) { step.motor = &m; }
    I_Motor* motor;
  };


  class SpinStep : public MotorStep
  {
  public:
    SpinStep(Step::StepType step) : MotorStep(step) {}
    virtual void run();
  };


  class WashStep : public MotorStep
  {
  public:
    WashStep(Step::StepType step) : MotorStep(step) {}
    virtual void run();
  };

} // namespace WashingMachine

#endif /* MOTORSTEP_H_ */
