// ----------------------------------------------------------------------------------
// GPIO.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#ifndef GPIO_H_
#define GPIO_H_

#include "peripherals.h"

// The Pin class represents an input/output pin on the
// STM32.  It may be used for GPIO (the default configuration)
// or to provide alternative I/O functionality (e.g. UART Tx/Rx)
//
namespace STM32F407
{
  class Pin
  {
  public:
    // Pin configuration
    //
    enum Mode        { INPUT, OUTPUT, ALT_FN, ANALOGUE };
    enum OutputType  { PUSH_PULL, OPEN_DRAIN };
    enum OutputSpeed { LOW, MEDIUM, HIGH, VERY_HIGH };
    enum PushPull    { NO_PUSH_PULL, PULL_UP, PUSH_DOWN };
    enum AltFunction { AF0, AF1, AF2,  AF3,  AF4,  AF5,  AF6,  AF7,
                       AF8, AF9, AF10, AF11, AF12, AF13, AF14, AF15};

    // Basic API
    //
    Pin(Peripheral::AHB1_Device dev, unsigned pin_number, Mode m = INPUT);
    void set();
    void clear();
    bool isSet();

    // Operator overloads
    //
    Pin& operator=(unsigned int val) { (val > 0)? set() : clear(); return *this; }
    operator unsigned int()          { return (isSet()? 1 : 0); }
    operator bool()                  { return isSet(); }

    // Configuration API - allows Pin function to be changed
    // after construction.
    //
    void mode(Mode m);
    void output_type(OutputType type);
    void output_speed(OutputSpeed speed);
    void push_pull(PushPull pupd);
    void alternative_function(AltFunction fn);

  private:
    // Copy policy
    //
    Pin(const Pin&);
    Pin& operator=(const Pin&);

    struct PortRegisters;
    volatile PortRegisters* port;

    unsigned int pin;
  };
} // namespace STM32F407

#endif /* GPIO_H_ */
