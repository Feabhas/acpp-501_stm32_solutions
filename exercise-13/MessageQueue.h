// ----------------------------------------------------------------------------------
// MessageQueue.h
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#ifndef MESSAGEQUEUE_H_
#define MESSAGEQUEUE_H_

#include "Buffer.h"
#include "Thread.h"

// MessageQueue acts as an adapter of
// the basic Buffer class, for use with
// multi-threaded code message passing.
// MessageQueue is a blocking element -
// that is, it will block if there is no data
// in its buffer.
//
using Utility::Buffer;

namespace feabhOS
{
  template <typename T = int, size_t sz = 16>
  class MessageQueue : private Buffer<T, sz>
  {
  public:
    MessageQueue() : Buffer<T, sz>() {}
    void post(const T& msg);
    T    get();
  };


  template <typename T, size_t sz>
  void MessageQueue<T, sz>::post(const T& msg)
  {
    Buffer<T, sz>::add(msg);
  }


  template <typename T, size_t sz>
  T MessageQueue<T, sz>::get()
  {
    while(Buffer<T, sz>::isEmpty())
    {
      Thread::yield(); // <= Give other threads a chance
    }
    T msg;
    Buffer<T, sz>::get(msg);
    return msg;
  }

} // namespace OS


#endif /* MESSAGEQUEUE_H_ */
