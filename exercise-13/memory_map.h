/*
 * memory_map.h
 *
 *  Created on: 26 Jun 2015
 *      Author: glennan
 */

#ifndef MEMORY_MAP_H_
#define MEMORY_MAP_H_

#if __cplusplus<201103L
  #include <stdint.h>
#else
  #include <cstdint>
  using std::uint32_t;
#endif

namespace STM32F407
{
  // Base address for devices on the STM32F10x
  //
  const uint32_t FLASH_BASE  = 0x08000000; // FLASH base address
  const uint32_t SRAM_BASE   = 0x20000000; // SRAM base address in the alias region
  const uint32_t PERIPH_BASE = 0x40000000; // Peripheral base address in the alias region

  // Peripheral memory map
  //
  const uint32_t APB1_BASE   = PERIPH_BASE + 0x00000; // Advanced Peripheral Bus 1
  const uint32_t APB2_BASE   = PERIPH_BASE + 0x10000; // Advanced Peripheral Bus 2
  const uint32_t AHB1_BASE   = PERIPH_BASE + 0x20000; // Advanced High-performance Bus 1

} // namespace STM32F407

#endif /* MEMORY_MAP_H_ */
