// ----------------------------------------------------------------------------------
// CheckedMotor.cpp
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include "CheckedMotor.h"
#include "Exceptions.h"
#include "Timer.h"

using namespace STM32F407;
using Utility::Exception;
using Utility::MotorFailed;

namespace WashingMachine
{
  CheckedMotor::CheckedMotor() :
    motor(),
    feedback(Peripheral::GPIO_D, 6, Pin::INPUT)
  {
  }

  void CheckedMotor::on()
  {
    motor.on();
    if(!isMotorRunning()) throw MotorFailed();
  }


  void CheckedMotor::off()
  {
    motor.off();
  }


  void CheckedMotor::direction(I_Motor::Direction d)
  {
    motor.direction(d);
  }


  bool CheckedMotor::isMotorRunning()
  {
    bool initialState = feedback;

    for (int i = 0; i < 1000; ++i)
    {
      bool newState = feedback;
      if(newState != initialState)
      {
        return true;
      }
      Timer::sleep(1);
    }
    return false;
  }

} // namespace WashingMachine
