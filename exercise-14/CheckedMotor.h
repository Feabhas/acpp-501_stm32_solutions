// ----------------------------------------------------------------------------------
// CheckedMotor.h
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#ifndef CHECKEDMOTOR_H_
#define CHECKEDMOTOR_H_

#include "Motor.h"
#include "GPIO.h"

using STM32F407::Pin;

// CheckedMotor acts as a 'Protection Proxy'
// for a Motor class.  It checks to see if the
// underlying motor object is working; if not
// it will throw an exception
//
namespace WashingMachine
{
  class CheckedMotor:  public I_Motor
  {
  public:
    CheckedMotor();
    virtual void on();
    virtual void off();
    virtual void direction(I_Motor::Direction d);

  protected:
    bool isMotorRunning();

  private:
    Motor motor;
    Pin feedback;
  };

} // namespace WashingMachine

#endif /* CHECKEDMOTOR_H_ */
