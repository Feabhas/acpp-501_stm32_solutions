// ----------------------------------------------------------------------------------
// WMS.h
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#ifndef WMS_H_
#define WMS_H_

#include <map>
#include "Thread.h"
#include "MessageQueue.h"

using feabhOS::MessageQueue;

namespace WashingMachine
{
  class WashProgramme;

  class WMS : public feabhOS::IRunnable
  {
  public:
    enum Programme { WHITE, COLOUR, MIXED, ECONOMY, USER_1, USER_2, NUM_PROGS };

    WMS();
    void set(Programme prog, WashProgramme& wash);
    void runProg(Programme prog);
    virtual bool run();            // <= From IRunnable

  private:
    typedef std::map<Programme, WashProgramme*> Prog_map;
    Prog_map progs;
    MessageQueue<Programme, 16> msgQueue;
  };

} // namespace WashingMachine

#endif /* WMS_H_ */
