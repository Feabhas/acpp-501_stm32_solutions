// ----------------------------------------------------------------------------------
// Description.cpp
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include <Description.h>
#include <utility>
#include <cstring>

using std::swap;
using std::strcpy;
using std::strlen;

namespace Utility
{
  Description::Description(const char* str) :
    text(0)
  {
    int len = strlen(str);
    if(len > 0)
    {
      text = new char[len + 1];
      strcpy(text, str);
    }
  }

  Description::~Description()
  {
    delete[] text;
  }


  Description::Description (const Description& other) :
    text(0)
  {
    if(other.text != 0)
    {
      text = new char[strlen(other.text) + 1];
      strcpy(text, other.text);
    }
  }


  Description& Description::operator=(Description rhs)
  {
    swap(*this, rhs);
    return *this;
  }


  void swap(Description& lhs, Description& rhs)
  {
    std::swap(lhs.text, rhs.text);
  }


  Description::operator const char*() const
  {
    if(text != 0) return text;
    else          return "";
  }

} // namespace Utility
