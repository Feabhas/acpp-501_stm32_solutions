// ----------------------------------------------------------------------------------
// Motor.h
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#ifndef MOTOR_H_
#define MOTOR_H_

#include "GPIO.h"

using STM32F407::Pin;

// Interface for Motor-like objects
//
namespace WashingMachine
{
  class I_Motor
  {
  public:
    enum Direction { CW, ACW };

    virtual void on() = 0;
    virtual void off() = 0;
    virtual void direction(Direction d) = 0;
    virtual ~I_Motor() {}
  };


  class Motor : public I_Motor
  {
  public:
    Motor();
    ~Motor();

    virtual void on();
    virtual void off();
    virtual void direction(I_Motor::Direction d);

   private:
    Pin ctrl;
    Pin dir;
  };

} // namespace WashingMachine

#endif /* MOTOR_H_ */
