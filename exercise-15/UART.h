// ----------------------------------------------------------------------------------
// UART.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------


#ifndef UART_H_
#define UART_H_

#include "stm32f4xx.h"
#include "peripherals.h"
#include "GPIO.h"
#include "Buffer.h"
#include "Interfaces.h"

using Utility::Buffer;
using WashingMachine::I_DigitInput;
using WashingMachine::I_DigitOutput;
using WashingMachine::I_TextOutput;

namespace STM32F407
{
  // UART identifier
  //
  enum UART_ID { UART_1, UART_2, UART_3, UART_4, USART_5, USART_6 };

  // -----------------------------------------------------------------
  // UART traits class declarations
  //

  // Base template.  Note there are NO definitions
  // in this template, as we should NEVER use it,
  // only the specialised classes.
  //
  template <UART_ID ID>
  class UART_traits
  {
  };

  // Template specialisation for UART 3
  //
  template<>
  class UART_traits<UART_3>
  {
  public:
    // Device settings
    //
    static const uint32_t address             = 0x40004800;
    static const IRQn_Type IRQ                = USART3_IRQn;

    // Tx/Rx pin remapping
    //
    static const Peripheral::AHB1_Device port = Peripheral::GPIO_B;
    static const unsigned int Tx_pin          = 10;
    static const unsigned int Rx_pin          = 11;
    static const Pin::AltFunction Tx_alt_func = Pin::AF7;
    static const Pin::AltFunction Rx_alt_func = Pin::AF7;

    // Configuration functions
    //
    static void enable_clock()
    {
      Peripheral::enable(Peripheral::USART_3);
    }
  };


  // -----------------------------------------------------------------
  // UART class declaration
  //
  template <UART_ID ID>
  class UART : public I_DigitOutput,
               public I_DigitInput,
               public I_TextOutput
  {
  public:
    UART();
    ~UART();

    void putChar(char c);
    char getChar();
    void putString(const char* str);

    static void IRQHandler(void);

    // NOTE:  These are free functions, NOT member
    //        functions.  However, they have been
    //        implemented inline.
    //        They could have also been implemented
    //        as member functions.
    //
    friend UART& operator<<(UART& uart, int in_val)     { uart.putChar(in_val); return uart; }
    friend UART& operator<<(UART& uart, const char* str){ uart.putString(str); return uart;  }
    friend void  operator>>(UART& uart, int& inout_val) { inout_val = uart.read();           }
    friend UART& operator<<(UART& uart, UART& (*modifier)(UART&)) { return modifier(uart);   }

  protected:
    // Interface implementation
    //
    virtual void write(int value) { putChar(value + '0'); }
    virtual void write(const char* str) { putString(str); }
    virtual int read() { return getChar() - '0'; }

  private:
    void remap_IO_Pins();

    // UART configuration registers
    //
    struct UARTRegisters;
    volatile UARTRegisters* uart;

    // Interrupt-driven UART
    //
    static UART* self;
    typedef Buffer<char, 8> Rx_Buffer_t;
    Rx_Buffer_t Rx_buffer;
  };


  // -----------------------------------------------------------------
  // UART class definitions
  //
  template <UART_ID ID>
  struct UART<ID>::UARTRegisters
    {
      unsigned SR;    /*!< USART Status register,                   Address offset: 0x00 */
      unsigned DR;    /*!< USART Data register,                     Address offset: 0x04 */
      unsigned BRR;   /*!< USART Baud rate register,                Address offset: 0x08 */
      unsigned CR1;   /*!< USART Control register 1,                Address offset: 0x0C */
      unsigned CR2;   /*!< USART Control register 2,                Address offset: 0x10 */
      unsigned CR3;   /*!< USART Control register 3,                Address offset: 0x14 */
      unsigned GTPR;  /*!< USART Guard time and prescaler register, Address offset: 0x18 */
    };

  template <UART_ID ID>
  UART<ID>::UART() :
    uart(reinterpret_cast<UARTRegisters*>(UART_traits<ID>::address)),
    Rx_buffer()
  {
    // Set up the callback pointer
    //
    self = this;

    // Stop the USART before configuring
    //
    uart->CR1 &= ~(1 << 13);

    // Enable the USART clock.  Since this operation is
    // specific to a particular device we cannot hard-code
    // a call here.  Use a function in the trait class to
    // perform the configuration for this particular device
    //
    UART_traits<ID>::enable_clock();

    // Reset STOP bits
    // USART3->CR2 &= ~(3 << 12);   // Clear STOP[13:12] bits

    // Setup 8,N,1
    //
    uart->CR1 |= ((1 << 2) | (1 << 3)); // Set TE and RE bits

    // 115.2kb based on 16MHz clk and 16x oversampling
    //
    uart->BRR = 0x8B;

    // Configure the Tx / Rx pins for the device
    //
    remap_IO_Pins();

    //  Enable interrupt on Rx
    //
    uart->CR1 |= (1 << 5);

    // Enable interrupts for this
    // particular device
    //
    NVIC_EnableIRQ(UART_traits<ID>::IRQ);

    // Enable the USART
    //
    uart->CR1 |= (1 << 13);
  }

  template <UART_ID ID>
  UART<ID>::~UART()
  {
    // Stop the USART
    //
    uart->CR1 &= ~(1 << 13);
  }

  template <UART_ID ID>
  void UART<ID>::putChar(char c)
  {
    while ((uart->SR & (1 << 7)) == 0)
    {
      ; // Wait...
    }
    uart->DR = c;
  }


  template <UART_ID ID>
  char UART<ID>::getChar(void)
  {
    char chr;
    Rx_Buffer_t::Error error;
    do
    {
      NVIC_DisableIRQ(UART_traits<ID>::IRQ);

      error = Rx_buffer.get(chr);

      NVIC_EnableIRQ(UART_traits<ID>::IRQ);
    } while(error != Rx_Buffer_t::OK);

    return chr;
  }


  template <UART_ID ID>
  void UART<ID>::putString(const char* str)
  {
    while (*str != '\0')
    {
      this->putChar(*str++);
    }
  }


  template <UART_ID ID>
  void UART<ID>::remap_IO_Pins()
  {
    // ----------------------------------------------
    // function setups are all taken from the traits
    // class for this UART ID.-
    // The GPIO port, Tx/Rx pins and the alternative
    // function setups are all taken from the traits
    // class for this UART ID.
    //
    // This function makes the assumptions:
    // -  A single GPIO port is used for both
    //    Tx and Rx
    // -  The UART's GPIO port is on AHB1
    //
    // This code is a demo of the use of traits
    // for template code configuration. It is not
    // meant to be the definitive solution for the
    // STM32F407
    // -----------------------------------------------

    // Enable GPIO port clock
    //
    Peripheral::enable(UART_traits<ID>::port);

    Pin Tx(UART_traits<ID>::port, UART_traits<ID>::Tx_pin);
    Pin Rx(UART_traits<ID>::port, UART_traits<ID>::Rx_pin);

    Tx.mode(Pin::ALT_FN);
    Tx.alternative_function(UART_traits<ID>::Tx_alt_func);
    Tx.output_speed(Pin::HIGH);
    Tx.push_pull(Pin::PULL_UP);

    Rx.mode(Pin::ALT_FN);
    Rx.alternative_function(UART_traits<ID>::Rx_alt_func);
    Rx.output_speed(Pin::HIGH);
    Rx.push_pull(Pin::PULL_UP);
  }


  // Definition of callback pointer
  //
  template <UART_ID ID>
  UART<ID>* UART<ID>::self = 0;


  template <UART_ID ID>
  void UART<ID>::IRQHandler(void)
  {
   uint8_t val = self->uart->DR;
   self->Rx_buffer.add(val);
  }

  // -----------------------------------------------------------------
  // Operator overloads
  //

  template <typename UART_Ty>
  UART_Ty& endl(UART_Ty& uart)
  {
    uart << "\n\r";
    return uart;
  }

} // namespace STM32F407

#endif /* UART_H_ */
