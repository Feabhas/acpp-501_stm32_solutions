// ----------------------------------------------------------------------------------
// WMS.cpp
// 
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include "WMS.h"
#include "WashProgramme.h"
#include "Exceptions.h"

using std::map;
using std::pair;
using Utility::Exception;

namespace WashingMachine
{
  WMS::WMS() :
    progs(),
    msgQueue()
  {
  }

  bool WMS::run()
  {
    Programme prog = msgQueue.get();

    Prog_map::iterator it = progs.find(prog);
    if(it != progs.end())
    {
      it->second->run();
    }

    return false;  // Run again.
  }


  void WMS::set(WMS::Programme prog, WashProgramme& wash)
  {
    progs.insert(Prog_map::value_type(prog, &wash));
  }


  void WMS::runProg(WMS::Programme prog)
  {
    msgQueue.post(prog);
  }

} // namespace WashingMachine
